FROM python:3.11.4

LABEL maintainer="Avengerist"
LABEL version=2.0.0

ENV ANSIBLE_VERSION=8.2.0
ENV ANSIBLE_LINT_VERSION=6.17.2

ENV MOLECULE_VERSION=5.1.0
ENV MOLECULE_PLUGINS_VERSION=23.4.1

ENV TERRAFORM_VERSION=1.5.4

ENV PACKER_VERSION=1.9.2

ENV DOCKER_VERSION=24.0.5
ENV DOCKER_COMPOSE_VERSION=2.26.1

ENV TERRAGRUNT_VERSION=0.55.20

ENV KUBECTL_VERSION=1.29.2

ENV HELM_VERSION=3.15.0

RUN apt-get update && apt-get install -y --no-install-recommends \
    wget \
    unzip \
    git \
    && pip install \
    ansible==${ANSIBLE_VERSION} \
    ansible_lint==${ANSIBLE_LINT_VERSION} \
    molecule==${MOLECULE_VERSION} \
    molecule-plugins[docker]==${MOLECULE_PLUGINS_VERSION} \
    && rm -rf /var/lib/apt/lists/* \ 
    && apt-get clean


ADD https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip .
ADD https://releases.hashicorp.com/packer/${PACKER_VERSION}/packer_${PACKER_VERSION}_linux_amd64.zip .
ADD https://download.docker.com/linux/static/stable/x86_64/docker-${DOCKER_VERSION}.tgz .
ADD https://github.com/gruntwork-io/terragrunt/releases/download/v${TERRAGRUNT_VERSION}/terragrunt_linux_amd64 .
ADD https://dl.k8s.io/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl .
ADD https://github.com/docker/compose/releases/download/v${DOCKER_COMPOSE_VERSION}/docker-compose-linux-x86_64 .
ADD https://get.helm.sh/helm-v${HELM_VERSION}-linux-amd64.tar.gz .

RUN unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip -d /bin && rm -f terraform_${TERRAFORM_VERSION}_linux_amd64.zip
RUN unzip packer_${PACKER_VERSION}_linux_amd64.zip -d /bin && rm -f packer_${PACKER_VERSION}_linux_amd64.zip
RUN tar xfvz docker-${DOCKER_VERSION}.tgz --strip 1 -C /bin docker/docker && rm -f docker-${DOCKER_VERSION}.tgz
RUN tar xfvz helm-v${HELM_VERSION}-linux-amd64.tar.gz --strip 1 -C /bin linux-amd64/helm && rm -f helm-v${HELM_VERSION}-linux-amd64.tar.gz
RUN mv ./docker-compose-linux-x86_64 /bin/docker-compose && chmod +x /bin/docker-compose
RUN mv ./terragrunt_linux_amd64 /bin/terragrunt && chmod +x /bin/terragrunt
RUN mv ./kubectl /bin && chmod +x /bin/kubectl

ENTRYPOINT ["/bin/bash", "-c"] 

